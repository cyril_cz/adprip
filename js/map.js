$(function() {
    //navigation bar
    $('#header').load('navigation.html')

    //stats notice
    $('#stats_disclaimer').load('stats_disclaimer.html')


    $('leaflet-control-fullscreen-button').attr('data-toggle', 'tooltip').attr('data-placement', 'right')

    $('[data-toggle="tooltip"]').tooltip()
})

var mapCenterDefault = [
        46.31564,
        3.562414,
    ], // center en France
    zoomDefault = 5.5,
    zoomMin = zoomDefault,
    zoomMax = 13,
    lat = url.searchParams.get('lat') || mapCenterDefault[0],
    lng = url.searchParams.get('lng') || mapCenterDefault[1],
    zoom = url.searchParams.get('zoom') || zoomDefault,
    mapCenter = [
        lat,
        lng,
    ],
    rateGradient = true,
    data_villes,
    allCommuneLayer = [], //tableau [dept feature, dept layer] pour chaque dept affichant les communes
    arrDptVotes,
    depLayer,
    highestVote

var activeOpacity = 0.8

var deptBorderWeight = 0.1
var deptBorderWeightHover = 0.5

var commBorderWeight = 0
var commBorderWeightHover = 0.5

var map = L.map('map', {
    preferCanvas: true,
    fullscreenControl: {
        pseudoFullscreen: true, // fullscreen to page width and height
        title: {
            false: 'Affichage plein écran',
            true: 'Quitter le plein écran',
        },
    },
    loadingControl: true,
}).setView(mapCenter, zoom)

map.on('zoomend', function(e) {
    if (map.getZoom() > 7) {
        $(".dept-label").show()
    } else {
        $(".dept-label").hide()
    }
})

// To support browser history
window.addEventListener('popstate', function(e) {
    if (e.state) {
        var latlng = e.state.latlng
        var zoom = e.state.zoom
        map.setView(latlng, zoom)
    } else {
        map.setView(mapCenter, zoomDefault)
    }
})

var carte_attribution = '© <a rel="nofollow noopener noreferrer" target="_blank" href="https://carto.com/attributions">CARTO</a> © <a rel="nofollow noopener noreferrer" target="_blank" href="https://openmaptiles.org/">OpenMapTiles</a> © <a rel="nofollow noopener noreferrer" target="_blank" href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>  contributors, '+
        'Soutiens: Licence Ouverte v1.0 <a target="_blank" rel="nofollow noopener noreferrer"  href="https://adprip.fr/archive/carte/">ADPRip.fr</a>, '+
        'geoJSON: <a rel="nofollow noopener noreferrer" target="_blank" href="/carto/COPYING">licence (ouverte)</a>, '+
        'Integration par <a rel="nofollow noopener noreferrer" target="_blank" href="https://twitter.com/emiliodib">@emiliodib</a>'

L.tileLayer('https://{s}.basemaps.cartocdn.com/light_nolabels/{z}/{x}/{y}{r}.png', {
    subdomains: 'abcd',
    maxZoom: 19,
    id: 'cartodb.nolabels',
    attribution: carte_attribution
}).addTo(map)

map.createPane('labels')
map.getPane('labels').style.zIndex = 650;
map.getPane('labels').style.pointerEvents = 'none';
L.tileLayer('https://{s}.basemaps.cartocdn.com/light_only_labels/{z}/{x}/{y}.png', {
    subdomains: 'abcd',
    maxZoom: 19,
    attribution: carte_attribution,
    pane: 'labels'
}).addTo(map);

map.attributionControl.setPrefix('<a href="https://leafletjs.com/" target="_blank" ref="nofollow noopener noreferrer">Leaflet</a>');

var infoContainer
var info = L.control({ position: 'topright' })
info.onAdd = function(map) {
    infoContainer = this._div = L.DomUtil.create('div', 'info')
    $(infoContainer).css('display', 'none')
    return this._div
}
info.addTo(map)

var legendNumber = L.control({ position: 'bottomleft' })
legendNumber.onAdd = function(map) {
    var div = L.DomUtil.create('div', 'info legend'),
        grades = [
            0,
            50,
            100,
            250,
            500,
            1000,
            5000,
            10000,
        ],
        labels = []
    div.innerHTML += '<h5>Nb. de soutiens</h5>'
    for (var i = 0; i < grades.length; i++) {
        div.innerHTML +=
            '<i style="background:' +
            getColorNumber(grades[i]) +
            '"></i> ' +
            prettyPrintNum(grades[i]) +
            (grades[i + 1] ? ' &ndash; ' + prettyPrintNum(grades[i + 1]) + '<br/>' : ' et +')
    }
    div.innerHTML += '<p class="help mt-2">Un facteur 10 est<br />appliqué pour les<br />départements.</p>'
    return div
}

var extentControl = L.Control.extend({
    options: {
        position: 'topleft',
    },
    onAdd: function(map) {
        var container = L.DomUtil.create('div', 'extentControl')
        container.title = "Afficher la France métropolitaine"
        $(container)
            .attr('data-toggle', 'tooltip')
            .attr('data-placement', 'right')
            .addClass('leaflet-bar leaflet-control')
            .css('background', 'white url(images/icons/france.png) no-repeat 50% 50%')
            .css('width', '26px')
            .css('height', '26px')
            .css('outline', '1px black')
            .css('border-radius', '4px')
            .css('box-shadow', '0 1px 5px rgba(0,0,0,0.65)')
            .css('cursor', 'pointer')
            .hover(
                function() {
                    $(this).css('background-color', '#f4f4f4')
                },
                function() {
                    $(this).css('background-color', 'white')
                }
            )
        // TO DO non-Safari
        // var linkEl = $(container).append("<a></a>")
        $(container).on('click', moveFranceMetropole)
        return container
    },
})
map.addControl(new extentControl())

var switchGradient = L.Control.extend({
    options: {
        position: 'topleft',
    },
    onAdd: function(map) {
        var container = L.DomUtil.create('div', 'switchgradientcontrol')
        container.title = 'Gradient de couleur par nombre ou par taux de soutiens'
        $(container)
            .addClass('leaflet-bar leaflet-control')
            .attr('data-toggle', 'tooltip')
            .attr('data-placement', 'right')
            .text('%')
            .css('background', 'white no-repeat 50% 50%')
            .css('width', '26px')
            .css('height', '26px')
            .css('outline', '1px black')
            .css('border-radius', '4px')
            .css('box-shadow', '0 1px 5px rgba(0,0,0,0.65)')
            .css('cursor', 'pointer')
            .css('padding-top', '1px')
            .css('padding-left', '4px')
            .css('font-weight', 'bold')
            .css('font-size', '16px')
            .hover(
                function() {
                    $(this).css('background-color', '#f4f4f4')
                },
                function() {
                    $(this).css('background-color', 'white')
                }
            )
            .on('click', function() {
                rateGradient = !rateGradient
                if (!rateGradient) {
                    $(this).text('#')
                    $(this).css('padding-left', '8px')
                    map.removeControl(legendPercent)
                    legendNumber.addTo(map)
                    for (var i = 0; i < allCommuneLayer.length; i++) {
                        allCommuneLayer[i][1].setStyle(function(feature) {
                            var heatMapColor = getColorNumber(feature.properties.soutiens)
                            return { fillOpacity: activeOpacity, weight: commBorderWeight, color: '#4d004b', fillColor: heatMapColor }
                        })
                    }
                    depLayer.setStyle(function(feature) {
                        if (feature.properties.hidden != 1) {
                            var heatMapColor = getColorNumber(feature.properties.soutiens / 10) // /10
                            return { fillOpacity: activeOpacity, weight: deptBorderWeight, color: '#4d004b', fillColor: heatMapColor } // color: '#4d004b',
                        } else {
                            return { fillOpacity: 0 }
                        }
                    })
                } else {
                    $(this).text('%')
                    $(this).css('padding-left', '4px')
                    legendPercent.addTo(map)
                    map.removeControl(legendNumber)
                    for (var i = 0; i < allCommuneLayer.length; i++) {
                        allCommuneLayer[i][1].setStyle(function(feature) {
                            var heatMapColor = getColor(feature.properties.pourcentage * 100)
                            return { fillOpacity: activeOpacity, weight: commBorderWeight, color: '#4d004b', fillColor: heatMapColor }
                        })
                    }
                    depLayer.setStyle(function(feature) {
                        if (feature.properties.hidden != 1) {
                            var heatMapColorDpt = getColor(feature.properties.pourcentage * 100)
                            return { fillOpacity: activeOpacity, weight: deptBorderWeight, color: '#4d004b', fillColor: heatMapColorDpt }
                        } else {
                            return { fillOpacity: 0 }
                        }
                    })
                }
            })
        return container
    },
})
map.addControl(new switchGradient())

function getColor(d) {
    return d >= 10
        ? '#4d004b'
        : d >= 5
            ? '#810f7c'
            : d >= 3
                ? '#88419d'
                : d >= 2 ? '#8c6bb1' : d >= 1.5 ? '#8c96c6' : d >= 1 ? '#9ebcda' : d >= 0.5 ? '#bfd3e6' : '#e0ecf4'
}

var switchLabels = L.Control.extend({
    options: {
        position: 'topleft',
    },
    onAdd: function(map) {
        var container = L.DomUtil.create('div', 'switchlabelscontrol')
        container.title = 'Afficher / cacher les noms de villes'
        $(container)
            .addClass('leaflet-bar leaflet-control text-center')
            .attr('data-toggle', 'tooltip')
            .attr('data-placement', 'right')
            .html('<i class="fa fa-tag"></i>')
            .css('background', 'white no-repeat 50% 50%')
            .css('width', '26px')
            .css('height', '26px')
            .css('outline', '1px black')
            .css('border-radius', '4px')
            .css('box-shadow', '0 1px 5px rgba(0,0,0,0.65)')
            .css('cursor', 'pointer')
            .css('font-weight', 'bold')
            .css('font-size', '15px')
            .hover(
                function() {
                    $(this).css('background-color', '#f4f4f4')
                },
                function() {
                    $(this).css('background-color', 'white')
                }
            )
            .on('click', function() {
                var pane = map.getPane('labels')
                if(pane.style.display === "none") {
                    pane.style.display = "block"
                } else {
                    pane.style.display = "none"
                }
            })
        return container
    },
})
map.addControl(new switchLabels())

var mairiesMarkers = new L.FeatureGroup()
if (url.searchParams.get('mairies') == 1) {
        map.addLayer(mairiesMarkers);
}
var switchMairiesMarkers = L.Control.extend({
    options: {
        position: 'topleft',
    },
    onAdd: function(map) {
        var container = L.DomUtil.create('div', 'switchmairiesmarkercontrol')
        container.title = 'Afficher / cacher les mairies recevant des CERFAs'
        $(container)
            .addClass('leaflet-bar leaflet-control text-center')
            .attr('data-toggle', 'tooltip')
            .attr('data-placement', 'right')
            .html('<i class="fa fa-file-text"></i>')
            .css('background', 'white no-repeat 50% 50%')
            .css('background-size', 'contain')
            .css('width', '26px')
            .css('height', '26px')
            .css('outline', '1px black')
            .css('border-radius', '4px')
            .css('box-shadow', '0 1px 5px rgba(0,0,0,0.65)')
            .css('cursor', 'pointer')
            .css('padding-top', '1px')
            .css('font-weight', 'bold')
            .css('font-size', '14px')
            .hover(
                function() {
                    $(this).css('background-color', '#f4f4f4')
                },
                function() {
                    $(this).css('background-color', 'white')
                }
            )
            .on('click', function() {
                if(map.hasLayer(mairiesMarkers)) {
                    map.removeLayer(mairiesMarkers);
                } else {
                    map.addLayer(mairiesMarkers);
                }
            })
        return container
    },
})
map.addControl(new switchMairiesMarkers())

var legendPercent = L.control({ position: 'bottomleft' })
legendPercent.onAdd = function(map) {
    var div = L.DomUtil.create('div', 'info legend'),
        grades = [
            0,
            0.5,
            1,
            1.5,
            2,
            3,
            5,
            10,
        ],
        labels = []
    div.innerHTML += '<h5>Taux de soutiens</h5>'
    // loop through density intervals and generate a label with a colored square for each interval
    for (var i = 0; i < grades.length; i++) {
        div.innerHTML +=
            '<i style="background:' +
            getColor(grades[i]) +
            '"></i> ' +
            grades[i] +
            (grades[i + 1] ? ' &ndash; ' + grades[i + 1] + '%<br/>' : '% et +')
    }
    return div
}
legendPercent.addTo(map)

var actionMarkers = new L.FeatureGroup()
if (url.searchParams.get('actions') != 0) {
        map.addLayer(actionMarkers);
}
var switchMarkers = L.Control.extend({
    options: {
        position: 'topleft',
    },
    onAdd: function(map) {
        var container = L.DomUtil.create('div', 'switchmarkercontrol')
        container.title = 'Afficher / cacher les actions de signons.fr'
        $(container)
            .addClass('leaflet-bar leaflet-control')
            .attr('data-toggle', 'tooltip')
            .attr('data-placement', 'right')
            .css('background', 'white url(images/icons/map_icon.png) no-repeat')
            .css('background-size', 'contain')
            .css('width', '26px')
            .css('height', '26px')
            .css('outline', '1px black')
            .css('border-radius', '4px')
            .css('box-shadow', '0 1px 5px rgba(0,0,0,0.65)')
            .css('cursor', 'pointer')
            .css('padding-top', '1px')
            .css('padding-left', '4px')
            .css('font-weight', 'bold')
            .css('font-size', '16px')
            .hover(
                function() {
                    $(this).css('background-color', '#f4f4f4')
                },
                function() {
                    $(this).css('background-color', 'white')
                }
            )
            .on('click', function() {
                if(map.hasLayer(actionMarkers)) {
                    map.removeLayer(actionMarkers);
                } else {
                    map.addLayer(actionMarkers);
                }
            })
        return container
    },
})
map.addControl(new switchMarkers())

var geocoder = L.Control.Geocoder.nominatim({
    // Limit set to 10 because we have 10 'Saint-Paul' in France
    geocodingQueryParams: { countrycodes: 'fr', featuretype: 'city', limit: 10 },
})

if (URLSearchParams && location.search) {
    // parse /?geocoder=nominatim from URL
    var params = new URLSearchParams(location.search)
    var geocoderString = params.get('geocoder')
    if (geocoderString && L.Control.Geocoder[geocoderString]) {
        console.log('Using geocoder', geocoderString)
        geocoder = L.Control.Geocoder[geocoderString]()
    } else if (geocoderString) {
        console.warn('Unsupported geocoder', geocoderString)
    }
}

var control = L.Control
    .geocoder({
        geocoder: geocoder,
        placeholder: 'Chercher une commune...',
        expand: 'click',
        defaultMarkGeocode: false,
        position: 'bottomright',
    })
    .on('markgeocode', function(e) {
        map.setView(e.geocode.center, 11)
        var latlngPoint = new L.LatLng(e.geocode.center.lat, e.geocode.center.lng)
        var polygonCible = leafletPip.pointInLayer(latlngPoint, depLayer, true)
        if (allCommuneLayer.length != 0) {
            var foundCommunes = false
            for (var i = 0; i < allCommuneLayer.length; i++) {
                var polygonCommune = leafletPip.pointInLayer(latlngPoint, allCommuneLayer[i][1], false)
                if (polygonCommune.length != 0) {
                    polygonCommune[0].fireEvent('click', {
                        latlng: [
                            e.geocode.center.lat,
                            e.geocode.center.lng,
                        ],
                    })
                    foundCommunes = true
                    i = allCommuneLayer.length
                }
            }
        }
        if (!foundCommunes) {
            polygonCible[0].fireEvent('click', {
                latlng: [
                    e.geocode.center.lat,
                    e.geocode.center.lng,
                ],
            })
            var commmuneClicked = false
            // TO DO
            // Callback
            setTimeout(function() {
                for (var i = 0; i < allCommuneLayer.length; i++) {
                    var polygonCommune = leafletPip.pointInLayer(latlngPoint, allCommuneLayer[i][1], false)
                    if (polygonCommune.length != 0) {
                        polygonCommune[0].fireEvent('click', {
                            latlng: [
                                e.geocode.center.lat,
                                e.geocode.center.lng,
                            ],
                        })
                        i = allCommuneLayer.length
                    }
                }
            }, 800)
        }
    })
    .addTo(map)

function getColorNumber(d) {
    return d >= 10000
        ? '#7f0000'
        : d >= 5000
            ? '#b30000'
            : d >= 1000
                ? '#d7301f'
                : d >= 500 ? '#ef6548' : d >= 250 ? '#fc8d59' : d >= 100 ? '#fdbb84' : d >= 50 ? '#fdd49e' : '#fee8c8'
}

var departements;
var outreMer;
var outreMerCom;
var outreMer987;
var outreMer988;
var currentDataTs;

$.when(
    $.getJSON('carto/departements.json', function(data) {
        departements = data;
    }),
    $.getJSON('carto/departements-97x.json', function(data) {
        outreMer = data;
    }),
    $.getJSON('carto/departements-98x.json', function(data) {
        outreMerCom = data;
    }),
    $.getJSON('carto/departements-987.json', function(data) {
        outreMer987 = data;
    }),
    $.getJSON('carto/departements-988.json', function(data) {
        outreMer988 = data;
    }),
    $.getJSON('carto/mairies.json', function(data) {
        for (var mIdx = 0; mIdx < data.length; mIdx++) {
            var circleMarker = L.circle([data[mIdx][1], data[mIdx][2]], 800,
                { color: '#3388ff' })
            circleMarker.bindPopup(
                '<h6>Vous pouvez d&eacute;poser un soutien papier (formulaire CERFA)'
                + ' &agrave; cette adresse :</h6>'
                + '<strong>' + data[mIdx][0].replace(' - ', '<br/>') + '</strong><br/><br/>'
                + '<div class="text-right"><a rel="nofollow noopener noreferrer" target="_blank" '
                + 'href="https://www.referendum.interieur.gouv.fr/formulaire-papier">'
                + 'En savoir plus...</a></div>')
            mairiesMarkers.addLayer(circleMarker)
        }
    }),
    $.get(urlADPRipCarte , function(data) { //get a list of available data timestamp
        currentDataTs = fillDataDateSelect(data, $('select#dataDate'))
    })
).then(function() {
    // Ajoute les outre-mers aux departements de la metropole
    Array.prototype.push.apply(departements.features, outreMer.features)
    Array.prototype.push.apply(departements.features, outreMerCom.features)
    Array.prototype.push.apply(departements.features, outreMer987.features)
    Array.prototype.push.apply(departements.features, outreMer988.features)

    departements.features.forEach(function(feature) {
        const position = getVillePosition(feature)
        var classes = 'dept-label dept-label-' + feature.properties.code
        if (feature.properties.nom.length > 10) classes += ' dept-label-long'
        var labelIcon = L.divIcon({className: classes, html:feature.properties.nom})
        var marker = new L.marker(position, { opacity: 0.6, icon: labelIcon })
        marker.addTo(map)
    })
    map.fireEvent('zoomend', {})

    reloadDataTS(currentDataTs, function(data_departements, comm) {
        setFrancaisEtranger(data_departements["00"])

        data_villes = comm

        // Ajoute les départements
        depLayer = generateDptLayer(data_departements).addTo(map)

        const insee = url.searchParams.get('insee')
        var latlngPointParams

        // regle la position si param url (exemple de la Somme : ?lat=49.9685061&lng=1.7309293&zoom=9)
        if (url.searchParams.get('lat') && url.searchParams.get('lng') && url.searchParams.get('zoom')) {
            latlngPointParams = new L.LatLng(lat, lng)
        }

        // Code insee fourni dans l'url
        var inseeLatLng = getLatLngFromInsee(insee)
        if(typeof inseeLatLng !== "string") {
            latlngPointParams = inseeLatLng
        }

        // Simule click sur departement (et commune si l'insee est fourni)
        if (latlngPointParams) {
            clickDeptInsee(latlngPointParams, insee)
        }

        // hide loading
        document.getElementById('parsing').style.display = 'none'
    })
})
// end of getJSON('carto/departements.json'...

$.getJSON('https://referendum.signons.fr/api/active-actions/', function(data) {
    var myIcon = L.icon({iconUrl: 'images/icons/map_icon.png', iconSize: [30, 30]});
    for(var sIdx = 0; sIdx < data["ldp:contains"].length; sIdx++) {
        var elem = data["ldp:contains"][sIdx];
        var id = elem["@id"].split("/").slice(-2, -1)[0];
        var popupText = '<div><h6>' + elem["name"] + '</h6>'
            + '<small>Action <a href="https://signons.fr/" rel="nofollow noopener noreferrer" target="_blank"><i>signons.fr</i></a> à ' + elem["city"] + ' le <b>' + elem["date"] + ' (' + elem["start_time"] + '~' + elem["end_time"] +')</b>.</small>'
            + '<p>' + convertlink(elem["description"]) + '</p>'
            + '<center><a href="https://referendum.signons.fr/les-actions/action-detail/@https~@~_~_referendum~!signons~!fr~_api~_actions~_'+id+'~_" rel="nofollow noopener noreferrer" target="_blank"><p class="participe">Je participe!</p></a></center>'
            + '</div>';

        if(data["ldp:contains"][sIdx]["lat"] != null && data["ldp:contains"][sIdx]["lng"] != null)
            actionMarkers.addLayer(L.marker([data["ldp:contains"][sIdx]["lat"],data["ldp:contains"][sIdx]["lng"]], {icon: myIcon}).bindPopup(popupText));
    }
})


var lastPopupDptCode = "00" //pour se souvenir de quel departement on a hover en dernier
function onEachFeature(feature, layer) {
    layer.on('click', function(e) {
        depLayerClick(feature, layer, e)
    })

    var isEstimation = (typeof feature.properties.homonymes !== 'undefined') && (feature.properties.homonymes.length > 0)
    var popupContent = '<div class="popup_grid"><div class="popup_blasons" style="background-image: url(\'images/blasons/'+feature.properties.code+'.png\'), url(\'images/blasons/'+getDepartementFromInsee(feature.properties.code)+'.png\')"></div>'
    popupContent += '<div class="popup_title"><h6 style="white-space: nowrap; margin-bottom:0px;">' + feature.properties.nom + '</h6>' +
        '<small><a rel="nofollow noopener noreferrer" target="_blank" href="https://www.referendum.interieur.gouv.fr/consultation_publique/8">listée comme</a> : '+feature.properties.nom_liste+'</small></div>'

    popupContent += '<div class="popup_stats">'
    if (feature.properties.electeurs > 0) {
        popupContent += 'Électeurs : ' + prettyPrintNum(feature.properties.electeurs) + '<br />' +
            'Soutiens acceptés'+ (isEstimation?' (estimation) ':' ')+ ': ' + prettyPrintNum(feature.properties.soutiens) + '<br />' +
            'Pourcentage'+ (isEstimation?' (estimation) ':' ')+ ': ' + Math.round(feature.properties.pourcentage * 10000) / 100 + ' %'
    } else if (feature.properties.electeurs == 0) {
        popupContent += 'Soutiens acceptés'+ (isEstimation?' (estimation) ':' ')+ ': ' + prettyPrintNum(feature.properties.soutiens) + '<br />' +
            'Électeurs : nombre d\'électeurs inconnu ou aucun électeur'
    } else {
        popupContent += 'Nombre d\'électeurs inconnu.<br />Autres communes avec même nom.<br />' +
            'Impossible d\'estimer le nombre de soutiens.'
    }

    popupContent += '</div><div class="text-muted small popup_notice">'

    if (typeof feature.properties.homonymes !== 'undefined') {
        if (feature.properties.homonymes.length > 0) {
            if (feature.properties.electeurs >= 0) {
                popupContent += "Les soutiens de cette commune sont estimés au prorata de ces communes (codes INSEE) : "
            } else {
                popupContent += 'Codes INSEE de ses communes homonymes : '
            }
            popupContent += JSON.stringify(feature.properties.homonymes)
                .replace(/[\[\]&\"]+/g, '')
                .replace(/[\,]+/g, ', ')
                .replace(/(\d*[AB]?\d+)/g, '<a onclick="mapClickInsee(\'$1\')" style="cursor: pointer; color: #0078A8;">$1</a>')
        }
    }
    popupContent += '</div>'

    var popupContentDpt =
        '<h6 style="white-space: nowrap; margin-top: 10px" class="text-center">' +
        feature.properties.nom + ' (' + feature.properties.code + ')' +
        '</h6>&Eacute;lecteurs : ' +
        prettyPrintNum(feature.properties.electeurs) +
        '<br/>Soutiens acceptés : ' +
        prettyPrintNum(feature.properties.soutiens) +
        '<br/>Pourcentage : ' +
        Math.round(feature.properties.pourcentage * 10000) / 100 +
        ' %'

    //COMs dont on a pas les donnees geoJSON par commune : ajout recherche commune par texte
    if(!hasGeoJSONCommuneData(feature)) {
        var nom = feature.properties.nom,
            latlng = getVillePosition(feature);
        popupContentDpt += "<br /><iframe id=\"domtomcom_fr\" frameBorder=\"0\" style=\"width:100%;height:100%;\" " +
                            "src=\"fuzzy_search.html?nocache&nom="+nom+"&code="+feature.properties.code+"&date="+currentDataTs+"\"></iframe>"
    }

    if (feature.properties.code.length > 3) {
        layer.bindPopup(popupContent + generateShareLinks(feature.properties.code, feature.properties.nom, getVillePosition(feature)) + "</div></div>", {minWidth: 250});
        layer.on('click', function(e) {
            // Add gps coordinates to window url
            if (e.latlng) {
                updateBrowserLocation(e.latlng, map.getZoom(), feature.properties.code)
            }
        })
        layer.on('mouseover', function(e) {
            layer.setStyle({
                weight: commBorderWeightHover,
            })
            var content = this._popup.getContent().replace(/<img[^>]*>/g,"");
            $(infoContainer).css('display', 'block')
            $(infoContainer).html(content)
        })
        layer.on('mouseout', function(e) {
            layer.setStyle({
                weight: commBorderWeight,
            })
            $(infoContainer).css('display', 'none')
        })
    } else {
        layer.on('mouseover', function(e) {
            layer.setStyle({
                weight: deptBorderWeightHover,
            })
            if(feature.properties.code != lastPopupDptCode) {
                layer.bindPopup(popupContentDpt)
                var content = this._popup.getContent()
                $(infoContainer).css('display', 'block')
                $(infoContainer).html(content)
                layer.unbindPopup()
                lastPopupDptCode = feature.properties.code
            }
        })
        layer.on('mouseout', function(e) {
            layer.setStyle({
                weight: deptBorderWeight,
            })

        if(hasGeoJSONCommuneData(feature)) { //hide commune search only if iframe doesn't exist
            $(infoContainer).css('display', 'none')
            lastPopupDptCode = "00"
        }
        })
    }
}

function updateBrowserLocation(latlng, zoom, insee = 0) {
    zoom = Math.max(Math.min(zoom, zoomMax), zoomMin)

    var newLocation = window.location.pathname
        + '?lat=' + parseFloat(latlng.lat || latlng[0]).toFixed(4)
        + '&lng=' + parseFloat(latlng.lng || latlng[1]).toFixed(4)
        + '&zoom=' + zoom;

    if (insee) {
        newLocation += '&insee=' + insee;
    } else if (url.searchParams.has('insee')) {
        newLocation += '&insee=' + url.searchParams.get('insee')
    }

    $("select#location").prop('selectedIndex', estimateMapPosition(latlng.lat || latlng[0], latlng.lng || latlng[1]))
    window.history.pushState(
        { latlng: latlng, zoom: zoom },
        null,
        newLocation
    )
}

function moveFranceMetropole() {
    map.setView(mapCenterDefault, zoomDefault)
    $("select#location").prop('selectedIndex', 0);

    // clean up url when back to default map
    window.history.pushState(
        { latlng: mapCenterDefault, zoom: zoomDefault },
        null,
        window.location.pathname
    )
}

function setFrancaisEtranger(data) {
    var fr_counter = prettyPrintNum(data.soutiens),
        fr_required = prettyPrintNum(data.electeurs),
        fr_percent = data.soutiens / data.electeurs * 100,
        fr_percentFormatted = new Intl.NumberFormat("fr-FR", { maximumFractionDigits: 2 }).format(fr_percent);

    $('#fr_etranger').text('Français de l\'Étranger : '+fr_counter+' soutiens / '+fr_required + ' (' + fr_percentFormatted + '%)');
}

function generateDptLayer(data_departements) {
    return L.geoJSON(departements, {
        style: function(feature) {
            feature.properties.soutiens = data_departements[feature.properties.code].soutiens
            feature.properties.electeurs = data_departements[feature.properties.code].electeurs;
            feature.properties.pourcentage = feature.properties.soutiens / feature.properties.electeurs
            allCommuneLayer.forEach(function(layer_pair) {
                if(feature.properties.code == layer_pair[0].properties.code) {
                    feature.properties.hidden = 1
                }
            })

            if (!rateGradient) {
                if (feature.properties.hidden != 1) {
                    var heatMapColor = getColorNumber(feature.properties.soutiens / 10) // /10
                    return { fillOpacity:activeOpacity, weight: deptBorderWeight, fillColor: heatMapColor } // color: '#4d004b',
                } else {
                    return { fillOpacity: 0, weight: 0 }
                }
            } else {
                if (feature.properties.hidden != 1) {
                    var heatMapColorDpt = getColor(feature.properties.pourcentage * 100)
                    return { fillOpacity: activeOpacity, weight: deptBorderWeight, color: '#4d004b', fillColor: heatMapColorDpt }
                } else {
                    return { fillOpacity: 0, weight: 0, color: '#4d004b' }
                }
            }
        },
        onEachFeature: onEachFeature,
    })
}

function depLayerClick(feature, layer, e) {
    var maxPrct = 0.01
    if (feature.properties.code.length <= 3) {
        var newZoom = Math.max(map.getZoom(), 7.5)
        // Add gps coordinates to window url
        if (typeof e === "object" && e.latlng) {
            updateBrowserLocation(e.latlng, newZoom)
        }

        if(!hasGeoJSONCommuneData(feature)) { //do not try to load geoJSON if we don't have it
            return
        }

        layer.closePopup()
        if(typeof e === "object") {
            map.setView(e.latlng, newZoom)
        }

        loadCommuneGeoJson(feature.properties.code, function(communeJson) {
            communeJson.features.forEach(function(feature) {
                var communeDataToAdd = data_villes[feature.properties.code]
                if (communeDataToAdd) {
                    feature.properties.electeurs = communeDataToAdd.electeurs
                    feature.properties.soutiens = communeDataToAdd.soutiens
                    feature.properties.pourcentage = feature.properties.soutiens / feature.properties.electeurs
                    feature.properties.homonymes = communeDataToAdd.homonymes
                    feature.properties.nom_liste = communeDataToAdd.nom

                    if (
                        feature.properties.electeurs == 0 ||
                        feature.properties.electeurs == undefined ||
                        feature.properties.pourcentage == undefined
                    ) {
                        feature.properties.pourcentage = 0
                    }

                    if (feature.properties.pourcentage > maxPrct) {
                        maxPrct = feature.properties.pourcentage
                    }
                }

            })

            layer.setStyle({
                fillOpacity: 0,
            })
            feature.properties.hidden = 1

            communeLayer = L.geoJSON(communeJson, {
                style: function(feature) {
                    if (!rateGradient) {
                        var heatMapColor = getColorNumber(feature.properties.soutiens)
                        return { fillOpacity: activeOpacity, weight: commBorderWeight, color: '#4d004b', fillColor: heatMapColor }
                    } else {
                        var heatMapColor = getColor(feature.properties.pourcentage * 100)
                        return { fillOpacity:activeOpacity, weight: commBorderWeight, color: '#4d004b', fillColor: heatMapColor }
                    }
                },
                onEachFeature: onEachFeature,
            })

            var isNewCommuneLayer = true
            for(var cIdx = 0; cIdx < allCommuneLayer.length; cIdx++) {
                if(allCommuneLayer[cIdx][0].properties.code == feature.properties.code) {
                    map.removeLayer(allCommuneLayer[cIdx][1])
                    map.addLayer(communeLayer)
                    allCommuneLayer[cIdx][1] = communeLayer
                    isNewCommuneLayer = false
                }
            }

            if (isNewCommuneLayer) {
                map.addLayer(communeLayer)
                allCommuneLayer.push([feature , communeLayer])
            }

            // Simule un click sur la commune si l'url contenait un code insee
            if (typeof e === "object" && e.insee && e.insee.length > 3) {
                const villeFound = communeJson.features.find(function(feature) {
                    return feature.properties.code == e.insee
                })
                if (villeFound) {
                    const position = getVillePosition(villeFound)
                    const latlngPointParams = new L.LatLng(position[0], position[1])
                    const polygonCommune = leafletPip.pointInLayer(latlngPointParams, communeLayer, false)
                    if (polygonCommune.length != 0) {
                        polygonCommune[0].fireEvent('click', { latlng: position })
                    }
                }
            }

            // Re-dessine les markers pour les mettre au dessus
            if (map.hasLayer(mairiesMarkers)) {
                map.removeLayer(mairiesMarkers)
                map.addLayer(mairiesMarkers)
            }
        })
    }
}

function getLatLngFromInsee(insee) {
    if(!insee) {
        return ""
    }

    const deptCode = getDepartementFromInsee(insee)
    const deptFound = departements.features.find(function(feature) {
        return feature.properties.code == deptCode
    })

    if (deptFound) {
        const position = getVillePosition(deptFound)
        return new L.LatLng(position[0], position[1])
    }

    return ""
}

function clickDeptInsee(latlngPointParams, insee) {
    const departementCible = leafletPip.pointInLayer(latlngPointParams, depLayer, true)
    if (departementCible.length > 0) {
        departementCible[0].fireEvent('click', {
            insee: insee,
            latlng: [
                latlngPointParams.lat,
                latlngPointParams.lng
            ]
        })
    } else {
        map.setView([latlngPointParams.lat, latlngPointParams.lng], zoom)
    }
    updateBrowserLocation(latlngPointParams, zoom, insee)
}

function mapClickInsee(insee) {
    latlng = getLatLngFromInsee(insee)
    clickDeptInsee(latlng, insee)
}
