'use strict';

const localeFR = d3.formatLocale({
    decimal: ",",
    thousands: " ",
    grouping: [3]
});

const format0dec = localeFR.format(".0f");
const format3dec = localeFR.format(".3f");
const formatPercent = localeFR.format(".3%");
const formatExponent = localeFR.format(".2e");
const formatThousands = localeFR.format(",");

const langs_fr = {
    "fr-fr": {
        "pagination": {
            "page_size": "Lignes/page",
            "first": "<<", //text for the first page button
            "first_title": "1ère page", //tooltip text for the first page button
            "last": ">>",
            "last_title": "Dernière page",
            "prev": "<",
            "prev_title": "Précédent",
            "next": ">",
            "next_title": "Suivant",
        },
    }
}

// créer un nouveau format pour les colonnes
Tabulator.prototype.extendModule("format", "formatters", {
    numberfmt: function (cell, formatterParams) {
        var column = cell.getColumn().getDefinition(),
            cellFormatted
        if (column.field === "Electeurs" || column.field === "Soutiens") {
            cellFormatted = localeFR.format(",")(cell.getValue())
        } else if (column.field === "Taux") {
            cellFormatted = localeFR.format(".2%")(cell.getValue() / 100)
        } else if (column.field === "Progression taux") {
            cellFormatted = localeFR.format(".3%")(cell.getValue() / 100)
        } else if (column.field === "objectif") {
            cellFormatted = localeFR.format(".2%")(cell.getValue())
        } else {
            cellFormatted = cell.getValue()
        }
        return cellFormatted
    },
    date: function (cell, formatterParams) {
        var column = cell.getColumn().getDefinition()
        return (column.field === "Date") ? moment(cell.getValue()).format("DD/MM/YYYY") : cell.getValue()
    }
});

// d3.schemeSpectral
var spectral = {
    1: "#9e0142",
    2: "#d53e4f",
    3: "#f46d43",
    4: "#fdae61",
    // 5: "#E2D678",
    // 6: "#ffffbf",
    // 7: "#e6f598",
    // 8: "#abdda4",
    5: "#66c2a5",
    // 10: "#3288bd",
    // 11: "#5e4fa2"
}
const rangColor = (rang) => (rang <= 10) ? spectral[rang] : "#3366cc"

function launchSparklines(jsonData, config, dates) {
    jsonData = jsonData.sort((a, b) => moment(a["Date"]).valueOf() - moment(b["Date"]).valueOf())

    var dateTop = moment(dates[dates.length - 1]).format("DD/MM/YYYY")

    var comparatif = function (electeurs, soutiens, dep) {
        var diffDays = (config.days_done - dates.length)
        var required = (electeurs * 0.1) / config.days_complete
        var cumul = required * diffDays
        var minRequired = [],
            cumulSoutiens = [],
            check = [],
            diff = 0;

        for (var i = 0; i < dates.length; i++) {
            diff = (cumul - soutiens[i]);
            (diff >= 0) ? check.push(false): check.push(true);
            minRequired.push(cumul);
            cumulSoutiens.push(soutiens[i]);
            cumul += required;
        }

        return {
            cumulSoutiens: cumulSoutiens,
            minRequired: minRequired,
            check: check
        }
    }

    d3.json("./carto/chart-listes-eu-2019.json").then(function (listesEU) {
        var nestedData = d3.nest()
            .key(d => d["Département"])
            .rollup(v => {
                var objCombine = {};
                v.forEach((d, i) => {
                    objCombine[d["Taux"]] = (d["Taux"] !== 0) ? rangColor(d["Rang progression (Top)"]) : rangColor('')
                })
                var objRank = {};
                v.forEach((d, i) => {
                    objRank[d["Taux"]] = rangColor(d["Rang taux"])
                })
                var objTop = v.map(d => d["Progression taux"] !== 0 ? rangColor(d["Rang progression (Top)"]) : rangColor(''))

                var electeurs = v.map(d => d["Electeurs"])
                var soutiens = v.map(d => d["Soutiens"])
                var bullet = [
                    [...new Set(electeurs)][0] * 0.1, d3.max(soutiens.sort()), [...new Set(electeurs)][0] * 0.1
                ]
                var objectif = bullet[1] / bullet[2]

                var listes_eu = v.map(d =>
                    (listesEU[d["Département"]] !== undefined) ? listesEU[d["Département"]].listes : []
                )
                var scores_eu = v.map(d => // [0.25, 0.25, 0.25, 0.10, 0.15]
                    (listesEU[d["Département"]] !== undefined) ? listesEU[d["Département"]].scores : []
                )
                var colorsListes = v.map(d => // [0.25, 0.25, 0.25, 0.10, 0.15]
                    (listesEU[d["Département"]] !== undefined) ? listesEU[d["Département"]].colors : []
                )

                // var difference = v.map(d => d["Diff"])
                var cumuls = comparatif(electeurs[0], soutiens, v.map(d => d["Département"]))
                var depts_num = [...new Set(v.map(d => d["Code dept"]))]

                return {
                    index: v.map((item, i) => i),
                    dep: v.map(d => d["Département"]),
                    depts_num: depts_num,
                    taux: v.map(d => d["Taux"]),
                    progress: v.map(d => d["Progression taux"]),
                    progress_soutiens: v.map(d => d["Progression soutiens"]),
                    soutiens: soutiens,
                    top: v.map(d => d["Rang progression (Top)"]),
                    rank: v.map(d => d["Rang taux"]),
                    date: v.map(d => d["Date"]),
                    bullet: bullet,
                    objectif: objectif,
                    colorsCombine: objCombine,
                    colorsRank: objRank,
                    colorsTop: objTop,
                    electeurs: electeurs,
                    listes_eu: listes_eu[0],
                    scores_eu: scores_eu[0],
                    colorsListes: colorsListes[0],
                    minRequired: cumuls.minRequired,
                    cumulSoutiens: cumuls.cumulSoutiens,
                    check: cumuls.check
                }
            })
            .entries(jsonData)

        var sparkData = []
        nestedData.forEach((item, i) => {
            sparkData.push({
                id: 1 + i,
                name: item.key,
                dep_num: item.value.depts_num[0],
                objectif: item.value.objectif,
                rank: item.value.rank[item.value.rank.length - 1],
                top: item.value.top[item.value.top.length - 1],
                check: item.value.check[item.value.check.length - 1],
                arr: item.value
            })
        })

        //generate bar chart
        var topFormatter = function (cell, formatterParams, onRendered) {
            onRendered(function () {
                $(cell.getElement()).sparkline(cell.getValue().progress, {
                    width: "100%",
                    type: "bar",
                    barWidth: 4,
                    highlightLighten: 1.8,
                    colorMap: cell.getValue().colorsTop,
                    date: cell.getValue().date,
                    dep: cell.getValue().dep,
                    top: cell.getValue().top,
                    progress: cell.getValue().progress,
                    progress_soutiens: cell.getValue().progress_soutiens,
                    tooltipFormatter: function (sp, options, fields) {
                        var x = fields[0].offset,
                            value = fields[0].value,
                            date = options.get('date')[x],
                            dep = options.get('dep')[x],
                            top = options.get('top')[x],
                            progress = options.get('progress')[x],
                            progress_soutiens = options.get('progress_soutiens')[x];

                        var format = '<div style="font-size: 14px;">';
                        format += '<h6>' + dep + '</h6>';
                        format += moment(date).format('LL');
                        format += '<br><span style="font-size: 30px; color:' + fields[0].color + '">&#9632;</span>&ensp;'
                        format += '<span>' + top + '<sup>e</sup> progression du jour</span>';
                        format += '<br><br> + ' + (Math.round(progress * 1000) / 1000).toLocaleString('fr') + '% (progression taux)';
                        format += '<br> + ' + formatThousands(progress_soutiens) + ' (progression soutiens)';
                        format += ' </div>'
                        return format;
                    }
                });
            });
        };

        var classementFormatter = (cell, formatterParams, onRendered) => {
            // console.log(cell)
            return cell.getValue() + ' <sup>e</sup>'
        }

        var top10Formatter = (cell, formatterParams, onRendered) => {
            // console.log(cell)
            return cell.getValue() + ' <sup>e</sup>'
        }

        var columnsGauge = [{
                title: (isMobile || isTablet) ? "Département (code)" : "Département",
                field: (isMobile || isTablet) ? "dep_num" : "name",
                width: (isMobile || isTablet) ? 80 : 200,
                align: (isMobile || isTablet) ? "center" : "left",
                frozen: true
            },
            {
                title: "Top* progression le " + dateTop,
                field: "top",
                width: (isMobile || isTablet) ? 80 : 120,
                align: "center",
                formatter: top10Formatter
            },
            {
                title: "Rang actuel",
                field: "rank",
                width: 120,
                align: "center",
                formatter: classementFormatter
            },
            {
                title: "% sur objectif (10% inscrits)",
                field: "objectif",
                width: 120,
                align: "center",
                formatter: "numberfmt"
            },
            {
                title: "Rang et progression par date",
                field: "arr",
                headerSort: false,
                formatter: topFormatter
            }
        ]

        //Table Constructor
        var tableGauge = new Tabulator('#evolution-info', {
            movableColumns: true,
            virtualDomBuffer: 150,
            height: 320,
            data: sparkData,
            placeholder: "Aucune donnée disponible",
            columns: columnsGauge,
            tooltipsHeader: true,
            initialSort: [{
                column: "top",
                dir: "asc"
            }],
            tableBuilt: () => setTimeout(() => $('#evolution-info-spinner').remove())
        });

        // partie additionnelle pour tableComplete
        var situationFormatter = function (cell, formatterParams, onRendered) {
            var rangeRequired = cell.getValue().minRequired.sort((a, b) => a - b)
            var rangeSoutiens = cell.getValue().cumulSoutiens.sort((a, b) => a - b)
            var difference = rangeRequired.map((d, i) => rangeSoutiens[i] - d)
            onRendered(function () {
                $(cell.getElement()).sparkline(_.difference(difference,
                    cell.getValue().cumulSoutiens.sort((a, b) => a - b)), {
                    type: 'bar',
                    barWidth: 1,
                    dep: cell.getValue().dep,
                    date: cell.getValue().date,
                    tooltipFormatter: function (sp, options, fields) {
                        var x = fields[0].offset,
                            value = fields[0].value,
                            dep = options.get('dep')[x],
                            date = options.get('date')[x];
                        var content =
                            (value >= 0) ?
                            ' + ' + formatThousands(Math.round(Math.abs(value))) + ' (en avance)' :
                            (value < 0) ?
                            ' - ' + formatThousands(Math.round(Math.abs(value))) + ' (en retard)' :
                            '   ' + formatThousands(Math.round(Math.abs(value))) + ' (objectif)';

                        var format = '<div style="font-size: 14px;">';
                        format += '<h6>' + dep + ' (soutiens)</h6>';
                        format += moment(date).format('LL');
                        format += '<br><br><span style="font-size: 18px; color:' + fields[0].color + '">&#9632;</span>&ensp;'
                        format += content;
                        format += '</div>';
                        return format;
                    }
                })
            })
        }

        var comparatifFormatter = function (cell, formatterParams, onRendered) {
            var rangeMaxSoutiens = d3.quantile(cell.getValue().cumulSoutiens.sort((a, b) => a - b), 0.9)
            onRendered(function () {
                $(cell.getElement()).sparkline(cell.getValue().minRequired, {
                    type: 'bar',
                    barWidth: 3,
                    barColor: '#aaf',
                    chartRangeMax: rangeMaxSoutiens,
                    dep: cell.getValue().dep,
                    date: cell.getValue().date,
                    soutiens: cell.getValue().cumulSoutiens,
                    tooltipFormatter: function (sp, options, fields) {
                        var x = fields[0].offset,
                            value = fields[0].value,
                            date = options.get('date')[x],
                            dep = options.get('dep')[x],
                            soutiens = options.get('soutiens')[x],
                            diff = (value - soutiens >= 0) ?
                            '<br><br> - ' + formatThousands(Math.round(Math.abs(value - soutiens))) + ' (retard)' :
                            '<br><br> + ' + formatThousands(Math.round(Math.abs(value - soutiens))) + ' (avance)';

                        var format = '<div style="font-size: 14px;">';
                        format += '<h6>' + dep + '<small> (soutiens)</small></h6>';
                        format += moment(date).format('LL');
                        format += '<br><br><span style="font-size: 18px; color:' + fields[0].color + '">&#9632;</span>&ensp;'
                        format += ' ' + formatThousands(Math.round(value)) + ' (nécessaires)';
                        format += '<br><span style="font-size: 18px; color:red">&#9632;</span>&ensp;'
                        format += ' ' + formatThousands(Math.round(soutiens)) + ' (publiés)';
                        format += diff;
                        format += '</div>'
                        return format;
                    }
                });
                $(cell.getElement()).sparkline(cell.getValue().cumulSoutiens, {
                    composite: true,
                    fillColor: false,
                    lineColor: 'red',
                    minRequired: cell.getValue().minRequired,
                    tooltipFormatter: function (sp, options, fields) {
                        return '';
                    },
                });
            });
        };

        //Formatter to generate pie chart
        var eu2019Formatter = function (cell, formatterParams, onRendered) {
            onRendered(function () {
                $(cell.getElement()).sparkline(cell.getValue().scores_eu, {
                    width: "100%",
                    type: 'bar',
                    barWidth: 10,
                    colorMap: cell.getValue().colorsListes,
                    listes: cell.getValue().listes_eu,
                    tooltipFormatter: function (sp, options, fields) {
                        var x = fields[0].offset,
                            value = fields[0].value,
                            liste = options.get('listes')[x];

                        var format = '<div style="font-size: 12px;">';
                        format += '<span style="font-size: 18px; color:' + fields[0].color + '">&#9632;</span>&ensp;'
                        format += listesEU[liste].abr + ' : ' + Math.round(value * 100) / 100 + ' %';
                        format += ' </div>'
                        return format;
                    }
                });
            });
        };
        //Formatter to generate bullet chart
        var bulletFormatter = function (cell, formatterParams, onRendered) {
            onRendered(function () {
                $(cell.getElement()).sparkline(cell.getValue().bullet, {
                    width: "100%",
                    type: "bullet",
                    targetWidth: 10,
                    targetColor: '#808000',
                    tooltipFormatter: function (sp, options, fields) {
                        var content =
                            (fields.fieldkey === "t") ? '<span style="font-size: 30px; color:#808000">&#9632;</span>&ensp;' +
                            formatThousands(Math.round(fields.value)) + ' = objectif' :
                            (fields.fieldkey === "p") ? '<span style="font-size: 30px; color:#3030f0">&#9632;</span>&ensp;' +
                            formatThousands(fields.value) + ' = soutiens actuels' :
                            '<span style="font-size: 30px; color:#7f94ff">&#9632;</span>&ensp;' + formatThousands(Math.round(fields.value)) +
                            ' = 10% élécteurs'

                        var format = '<div style="font-size: 1rem;">';
                        format += content;
                        format += ' </div>'
                        return format;
                    },
                });
            });
        };

        //generate bar chart
        var rankFormatter = function (cell, formatterParams, onRendered) {
            onRendered(function () {
                $(cell.getElement()).sparkline(cell.getValue().taux, {
                    width: "100%",
                    type: "bar",
                    barWidth: 3,
                    highlightLighten: 1.8,
                    colorMap: $.range_map(cell.getValue().colorsRank),
                    date: cell.getValue().date,
                    dep: cell.getValue().dep,
                    rank: cell.getValue().rank,
                    soutiens: cell.getValue().soutiens,
                    electeurs: cell.getValue().electeurs,
                    tooltipFormatter: function (sp, options, fields) {
                        var x = fields[0].offset,
                            value = fields[0].value,
                            date = options.get('date')[x],
                            dep = options.get('dep')[x],
                            rank = options.get('rank')[x],
                            soutiens = options.get('soutiens')[x],
                            electeurs = options.get('electeurs')[x];

                        var format = '<div style="font-size: 14px;">';
                        format += '<h6>' + dep + '</h6>';
                        format += moment(date).format('LL');
                        format += '<br><br><span style="font-size: 30px; color:' + fields[0].color + '">&#9632;</span>&ensp;'
                        format += '<span>' + rank + '<sup>e</sup> rang par taux</span>';
                        format += '<br>' + (Math.round(value * 100) / 100).toLocaleString('fr') + ' % taux de soutiens';
                        format += '<br><br>' + formatThousands(electeurs) + ' incrits';
                        format += '<br>' + formatThousands(soutiens) + ' soutiens';
                        format += ' </div>'
                        return format;
                    }
                });
            });
        };

        document.getElementById('selectCharts').addEventListener('change', function (e) {
            $('#toolbar').hide()
            var footerContent = '<div class="tabulator-footer">';
            footerContent += '<span class="float-left"><a type="button" href="https://github.com/olifolkerd/tabulator" target="_blank" '
            footerContent += 'style="padding: 2px 5px; font-weight: 900">Tabulator</a></span>'
            footerContent += '<span class="float-left mr-4" style="padding: 2px 5px">'
            footerContent += '<small><a href="https://omnipotent.net/jquery.sparkline/#s-about" target="_blank" '
            footerContent += '>with jQuery Sparklines</a></small></span>'
            footerContent += '<span class="float-left" style="padding: 2px 5px"><small>'
            footerContent += '<a href="https://www.interieur.gouv.fr/Elections/Les-resultats/Europeennes/elecresult__europeennes-2019/(path)/europeennes-2019/index.html"'
            footerContent += ' target="_blank">** listes &ge; 5%</a></small></span>';
            footerContent += '<span class="mr-2">Rang</span>';
            footerContent += '<span class="badge mr-2" style=" background-color: ' + rangColor(1) + '">&ensp;</span><span class="mr-2">1<sup>e</sup></span>';
            footerContent += '<span class="badge mr-2" style="background-color: ' + rangColor(2) + '">&ensp;</span><span class="mr-2">2<sup>e</sup></span>';
            footerContent += '<span class="badge mr-2" style="background-color: ' + rangColor(3) + '">&ensp;</span><span class="mr-2">3<sup>e</sup></span>';
            footerContent += '</div>';

            var columnsComplete = [{
                    title: "Code",
                    field: "dep_num",
                    width: 80,
                    align: "center",
                    frozen: true
                },
                {
                    title: "Département",
                    field: "name",
                    width: 200,
                    frozen: true
                },
                {
                    title: "Rang actuel",
                    field: "rank",
                    width: 100,
                    align: "center",
                    formatter: classementFormatter
                },
                {
                    title: "% sur objectif (10% inscrits)",
                    field: "objectif",
                    width: 120,
                    align: "center",
                    formatter: "numberfmt"
                },
                {
                    title: "EU 2019**",
                    field: "arr",
                    width: 100,
                    headerSort: false,
                    formatter: eu2019Formatter
                },
                {
                    title: "Soutiens sur objectif",
                    field: "arr",
                    width: 100,
                    headerSort: false,
                    formatter: bulletFormatter
                },
                {
                    title: "Situation par date",
                    field: "arr",
                    headerSort: false,
                    formatter: situationFormatter
                },
                {
                    title: "Soutiens par date",
                    field: "arr",
                    align: "center",
                    headerSort: false,
                    formatter: comparatifFormatter
                },
                {
                    title: "Taux et rang par date",
                    field: "arr",
                    headerSort: false,
                    formatter: rankFormatter
                },
                {
                    title: "Top progression le " + dateTop,
                    field: "top",
                    width: 80,
                    align: "center",
                    formatter: top10Formatter
                },
                {
                    title: "Rang et progression par date",
                    field: "arr",
                    headerSort: false,
                    formatter: topFormatter
                }
            ]

            var tableComplete = new Tabulator("#sparklinesDiv", {
                movableColumns: true,
                locale: true,
                langs: langs_fr,
                selectable: true,
                height: 650,
                virtualDomBuffer: 200,
                data: sparkData,
                placeholder: "Aucune donnée disponible",
                columns: columnsComplete,
                footerElement: footerContent,
                tooltipsHeader: true,
                initialSort: [{
                    column: "rank",
                    dir: "asc"
                }]
            });
            // tableComplete.redraw(true)

            function matchAny(data, filterParams) {
                var match = false;
                for (var key in data) {
                    if (JSON.stringify(_.toLower(data[key])).search(_.toLower(filterParams)) != -1) {
                        match = true;
                    }
                }
                return match;
            }
            //set filter to custom function
            $("#filter-sparklines").keyup(function (e) {
                // console.log(e)
                tableComplete.setFilter(matchAny, $("#filter-sparklines").val());
                if ($("#filter-sparklines").val() == " ") {
                    tableComplete.clearFilter()
                }
            });

            document.getElementById('filter-clear').onclick = function (e) {
                document.getElementById('filter-sparklines').value = ""
                tableComplete.clearFilter(true)
            }

            // deselect row on "deselect all" button click
            document.getElementById('deselectAll-sparklines').onclick = function (e) {
                tableComplete.deselectRow();
            };
        }, false) // de selectCharts

    }) // fin de listes EU
    console.log("done !")
} // fin launchSparklines
